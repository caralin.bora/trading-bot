const API = require("../service/api");
const CSV = require("../service/csv");

const CANDLESTICK_INTERVAL = 300; // 5 min;
const BINANCE_REQUEST_LIMIT = 1000;

class Service {

    static async loadHistoryForLastDays(numberOfDays = 1) {

        // TODO: set
        // this.product

        const startDate = new Date();
        const endDate = new Date(startDate)
        endDate.setDate(endDate.getDate() - numberOfDays);


        console.log(` ---- Split the interval in chunks of 1000 candles of 5 min ----`);
        const intervals = this._calculateIntervalsFor(startDate, endDate);
        console.log(` Got ${intervals.length} intervals`);
        console.log();


        console.log(` ---- Get data for last ${numberOfDays} days from DB or API----`);
        let candles = [];

        for (let i = 0; i < intervals.length; i++) {
            // TODO: search if interval Data found in DB
            // based on: product, interval and start/end time

            const res = await API.getPastSticks({
                symbol: this.product,
                interval: CANDLESTICK_INTERVAL,
                startTime: intervals[i].start,
                endTime: intervals[i].end
            });
            candles = candles.concat(res);
        }

        // console.log(candles[0].averageHL2());
        // console.log(candles[1].futurePercentChange());
        // console.log(candles[2]);

        console.log(` Got ${candles.length} candles \n`);

        console.log();

        return candles;
    };

    static async formatDataForML(candles) {

        console.log(` ---- Formatting the data for Machine Learning ----`);

        const X = [];
        const Y = [];

        candles.forEach((candle) => {
            X.push({
                open: candle.open,
                close: candle.close,
                futureClose: candle.futureClose,
                highToOpen: candle.highToOpen(),
                lowToOpen: candle.lowToOpen(),
                closeToOpen: candle.closeToOpen(),
                highToLow: candle.highToLow(),
                volume: candle.volume,
                futurePercentChange: candle.futurePercentChange(),
            });
            Y.push({
                futurePercentChange: candle.futurePercentChange(),
            });
        });

        console.log();

        return {
            X,
            Y
        }
    }

    static async exportMLDataToCSV(MLData) {
        console.log(` ---- Writing the ML data to CSV files ----`);

        CSV.writeCSVs(MLData);

        console.log();
    }

    static _calculateIntervalsFor(startDate, endDate) {
        // reverse the start and end time
        const localEndDate = startDate;
        const localStartDate = endDate;

        const intervalDelta = (1000 * CANDLESTICK_INTERVAL * BINANCE_REQUEST_LIMIT);
        const intervals = [];
        let start = localStartDate;

        do {

            let endMillis = start.getTime() + intervalDelta;

            if (endMillis > localEndDate.getTime()) {
                endMillis = localEndDate.getTime();
            }
            const end = new Date(endMillis);

            intervals.push({
                start,
                end
            });

            start = end;
        } while (start.getTime() !== localEndDate.getTime());

        return intervals;
    }
}

module.exports = {
    Data: Service
};

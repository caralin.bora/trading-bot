// TODO: create CSV files and write data to them
const CSVWriter = require('csv-writer');

module.exports = {
    writeCSVs: (MLData, extraFileName) => {

        const nowTime = new Date();
        const timeLabel = `${nowTime.getFullYear()}-${nowTime.getMonth()+1}-${nowTime.getDate()}`;

        const XMLData = MLData.X;
        // const YMLData = MLData.Y;

        const createCsvWriter = CSVWriter.createObjectCsvWriter;
        const XDataCSVWriter = createCsvWriter({
            path: `output/${extraFileName || ''}${timeLabel}.csv`,
            header: [
                {id: 'open', title: 'Open'},
                {id: 'close', title: 'Close'},
                {id: 'futureClose', title: 'Future Close'},
                {id: 'highToOpen', title: 'High To Open'},
                {id: 'lowToOpen', title: 'Low To Open'},
                {id: 'closeToOpen', title: 'Close To Open'},
                {id: 'highToLow', title: 'High To Low'},
                {id: 'volume', title: 'Volume'},
                {id: 'futurePercentChange', title: 'Future Percent Change'}
            ]
        });
        XDataCSVWriter.writeRecords(XMLData).then(()=> console.log(' The data CSV file was written successfully'));

    }
}

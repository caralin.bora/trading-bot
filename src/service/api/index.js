const CandleStick = require("../../model/candlestick");
const Binance = require('binance-api-node').default

class API {

    static async getPastSticks({symbol, interval, startTime, endTime}) {

        const product = symbol || 'ETHEUR';

        const client = Binance();
        let intervalString;

        switch (interval) {
            case 60:
                intervalString = '1m';
                break;
            case 300:
                intervalString = '5m';
                break;
            case 900:
                intervalString = '15m';
                break;
            case 3600:
                intervalString = '1h';
                break;
            case 21600:
                intervalString = '6h';
                break;
            case 86400:
                intervalString = '1d';
                break;
            default:
                intervalString = '5m';
        }

        const startDate = startTime || new Date();
        const previousDayDate = new Date(startDate);
        previousDayDate.setDate(previousDayDate.getDate() - 1);

        const response = await client.candles({
            symbol: product,
            limit: 1000,
            interval: intervalString,
            startTime: startTime.getTime(),
            endTime: endTime ? endTime.getTime() : previousDayDate.getTime(),
        });

        const candleSticksList = [];

        for (let i = 0; i < response.length; i++) {

            const time = new Date(response[i].openTime);
            //
            // const candleStickFromDB = await CandleStick.findOne({startTime: time, interval: intervalString});
            //
            // if(candleStickFromDB) {
            //     candleSticksList.push(candleStickFromDB);
            // } else {

                // const candleStick = await CandleStick.create({
                const candleStick = new CandleStick({
                    product,
                    low: parseFloat(response[i].low),
                    high: parseFloat(response[i].high),
                    close: parseFloat(response[i].close),
                    open: parseFloat(response[i].open),
                    interval: intervalString,
                    volume: parseFloat(response[i].volume),
                    startTime: time,
                    futureClose: parseFloat(response[(i === response.length - 1) ? 0 : i + 1].close),
                });

                // candleStick.save();

                candleSticksList.push(candleStick);
            // }
        }

        return candleSticksList;
    }
}

module.exports = API;

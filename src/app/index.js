const Database = require('../service/db');
const Service = require('../service');

const TIME_INTERVAL = 5 * 60 * 1000 // minutes * seconds * milliseconds

const generateMLDataCSV = async (numberOfDays) => {

    console.log(' ');
    console.log(' ################ GENERATE ML DATA ############### ');
    console.log(' ');

    try {
        const candles = await Service.Data.loadHistoryForLastDays(numberOfDays);
        const MLData = await Service.Data.formatDataForML(candles);
        await Service.Data.exportMLDataToCSV(MLData);

    } catch (error) {
        console.log(error);
    }
}

const localTick = async () => {
    try {
        const candles = await Service.Data.loadHistoryForLastDays(1);

        console.log();
        console.log(candles[candles.length - 1 ].prettyPrint());
        console.log();

    } catch (error) {
        console.log(error);
    }
}

const mainLoop = () => {


    console.log(' ');
    console.log(' ################     RUN APP     ############### ');
    console.log(' ');

    setInterval(localTick, TIME_INTERVAL);

}

module.exports = {
    start: async () => {

        console.log(' ################ INITIALIZE APP ################ ');
        console.log(' ');
        console.log(' - Connect to Mongo DB: ');
        await Database.connect();

        // Other init calls

        console.log(' ');
        console.log(' # INITIALIZE DONE ');
        console.log(' ');

        // await mainLoop();
        await generateMLDataCSV(90);
    }
}

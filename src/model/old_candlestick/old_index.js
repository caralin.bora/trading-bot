class CandleStick {
    constructor({low, high, close, open, interval, startTime = new Date(), volume, futureClose}) {
        this.low = low;
        this.high = high;
        this.close = close;
        this.open = open;
        this.interval = interval;
        this.startTime = startTime;
        this.volume = volume || 0;
        this.state = close ? 'closed' : 'open'

        // ML data - X
        this.highToOpen = high / open - 1;
        this.lowToOpen = low / open - 1;
        this.closeToOpen = close / open - 1;
        this.highToLow = high / low - 1;

        // ML data - Y
        // TODO: wrong calculation ?
        this.futurePercenteChange = 1 - close / futureClose;

    }

    averageHLC3() {
        return (this.close + this.high + this.low) / 3;
    }

    averageHL2() {
        return (this.high + this.low) / 2;
    }
}

module.exports = CandleStick;

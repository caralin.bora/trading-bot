const mongoose = require('mongoose');
const {Schema} = require('mongoose');

const DB_MODEL = 'Candlestick';

const CandlestickSchema = new Schema({

    product: String,
    low: Number,
    high: Number,
    close: Number,
    futureClose: Number,
    open: Number,
    interval: String,
    startTime: {
        type: Number,
        createIndexes: true,
        required: true,
    },
    volume: Number,
    state: {
        type: String,
        default: 'closed',
    }

});

CandlestickSchema.methods = {
    // utils
    averageHL2 : function () {
        return (this.high + this.low) / 2;
    },
    averageHLC3 : function () {
        return (this.close + this.high + this.low) / 3;
    },

    prettyPrint : function () {
        const startTime = new Date(this.startTime);

        return `${startTime} - open: ${this.open}, close: ${this.close} - futureClose: ${this.futureClose}`;
    },

    // ML data - X
    highToOpen: function () {
        return (this.high / this.open - 1).toFixed(10);
    },
    lowToOpen: function () {
        return (this.low / this.open - 1).toFixed(10);
    },
    closeToOpen: function () {
        return (this.close / this.open - 1).toFixed(10);
    },
    highToLow: function () {
        return (this.high / this.low - 1).toFixed(10);
    },

    // ML data - Y
    futurePercentChange: function () {
        return (1 - this.close / this.futureClose).toFixed(10);
    }

}


const Candlestick = mongoose.model(DB_MODEL, CandlestickSchema);
module.exports = Candlestick;

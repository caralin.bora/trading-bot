import io
import pandas as pd

from google.colab import files

X_file_uploaded = files.upload()
x_data = pd.read_csv(io.BytesIO(X_file_uploaded['X--2020-11-16.csv']))

Y_file_uploaded = files.upload()
y_data = pd.read_csv(io.BytesIO(Y_file_uploaded['Y--2020-11-16.csv']))

x_data
y_data
